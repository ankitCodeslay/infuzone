//
//  NetworkManager.swift
//  Infuzone
//
//  Created by Manish on 19/11/16.
//  Copyright © 2016 Codeslay technologies. All rights reserved.
//

import UIKit
import Alamofire

let kBaseURL:String = "http://codeslay.com/Infuzone/v1/index.php/"
let kImageBaseURL:String = "http://codeslay.com/Infuzone/images/"

class NetworkManager: NSObject {
//    class func getAllCategories(){
    class func getAllCategories(completion: @escaping (_ result: Array<Any>) -> Void) {
        Alamofire.request(kBaseURL+"getCategories").responseJSON { response in
            if let jsonResult = response.result.value as? Dictionary<String, AnyObject> {
                if(jsonResult["error"] as? Bool == false){
                    completion(jsonResult["message"] as! Array<Any>)
                }
                
            }
            else{
                print("No Json in ")
                completion([])
            }
        }
    }
    
    class func getNewsForCategory(category:String, modified_at:String, completion: @escaping (_ result: Array<Any>) -> Void) {
        print(Optional(category)!)
        
        Alamofire.request(kBaseURL+"getNews?category_id="+category+"&modified_at="+modified_at).responseJSON { response in
            if let jsonResult = response.result.value as? Dictionary<String, AnyObject> {
                if(jsonResult["error"] as? Bool == false){
                    completion(jsonResult["message"] as! Array<Any>)
                }
                else{
                    print("Error")
                    completion([])
                }
            }
            else{
                print("No Json in ")
                completion([])
            }
        }
    }
    
    class func getCommentsForNews(news:String, completion: @escaping (_ result: Array<Any>) -> Void) {
        print(kBaseURL+"getSpecificNewsComment?news_id="+news)
        Alamofire.request(kBaseURL+"getSpecificNewsComment?news_id="+news).responseJSON { response in
            if let jsonResult = response.result.value as? Dictionary<String, AnyObject> {
                if(jsonResult["error"] as? Bool == false){
                    completion(jsonResult["message"] as! Array<Any>)
                }
                else{
                    print("Error")
                    completion([])
                }
            }
            else{
                print("No Json in ")
                completion([])
            }
        }
    }
    
    class func postCommentWithNewsId(newsId:String, name:String, comment:String, rating:Float, completion: @escaping (_ success: Bool) -> Void) {
        let parameters: Parameters = [
            "news_id": newsId,
            "comment": comment,
            "name": name,
            "rating": String(rating)
        ]
        Alamofire.request(kBaseURL+"addComment", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            if let jsonResult = response.result.value as? Dictionary<String, AnyObject> {
                if(jsonResult["error"] as? Bool == false){
                    completion(true)
                }
                else{
                    print("Error")
                    completion(false)
                }
            }
            else{
                print("No Json in ")
                completion(false)
            }
        }
    }
}
