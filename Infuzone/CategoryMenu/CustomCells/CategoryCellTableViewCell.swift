//
//  CategoryCellTableViewCell.swift
//  Ifuzone
//
//  Created by Manish on 10/11/16.
//  Copyright © 2016 Codeslay technologies. All rights reserved.
//

import UIKit
import AlamofireImage
class CategoryCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var categoryImageView: UIImageView?
    @IBOutlet weak var categoryTitle: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(categoryDict:Dictionary<String, Any>){
        categoryTitle?.text = categoryDict["name"] as? String
        let url = URL(string: kImageBaseURL+(categoryDict["image"] as! String))!
        let placeholderImage = UIImage(named: "logo")!
        categoryImageView?.af_setImage(withURL: url, placeholderImage: placeholderImage)
    }
    
}
