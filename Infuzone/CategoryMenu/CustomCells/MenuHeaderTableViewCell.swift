//
//  MenuHeaderTableViewCell.swift
//  Ifuzone
//
//  Created by Manish on 10/11/16.
//  Copyright © 2016 Codeslay technologies. All rights reserved.
//

import UIKit

class MenuHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet var logoImageView: UIImageView?

    override func awakeFromNib() {
        super.awakeFromNib()
        logoImageView?.layer.borderColor = UIColor.white.cgColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
