//
//  MenuCategoryViewController.swift
//  Ifuzone
//
//  Created by Manish on 10/11/16.
//  Copyright © 2016 Codeslay technologies. All rights reserved.
//

import UIKit
import KYDrawerController

class MenuCategoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableview:UITableView?
    var categoryArray:Array<Any>?
    var currentView:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview?.register(UINib(nibName: "MenuHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "headerCell")
        tableview?.register(UINib(nibName: "CategoryCellTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.updateCategory()
        // Do any additional setup after loading the view.
    }
    
    func updateCategory(){
        categoryArray = self.getFromPlist()
        tableview?.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - TableView Datasource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1 + (categoryArray?.count)!;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if(indexPath.row == 0){
            let cell:MenuHeaderTableViewCell = tableview?.dequeueReusableCell(withIdentifier: "headerCell", for: indexPath) as! MenuHeaderTableViewCell
            return cell;
        }
        else{
            let cell:CategoryCellTableViewCell = tableview?.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CategoryCellTableViewCell
            let dict = categoryArray?[indexPath.row-1] as! Dictionary <String,Any>
            cell.setupCell(categoryDict: dict)
            return cell;
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let drawerCtrl:KYDrawerController = appDelegate.window?.rootViewController as! KYDrawerController
        
        if (currentView==indexPath.row) {
            drawerCtrl.setDrawerState(.closed, animated: true)
        }else if(indexPath.row==0){
            drawerCtrl.setDrawerState(.closed, animated: true)
        }else {
            currentView = indexPath.row;
            let dict = categoryArray?[indexPath.row-1] as! Dictionary <String,Any>
            let newsDisplayViewController = NewsScrollPageViewController(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal)
            if let id = dict["id"] as? Int {
                let category_id:String! = String(id)
                var modified_at:String! = String(id)
                modified_at = "0"
                
                newsDisplayViewController.category_id = category_id
                newsDisplayViewController.modified_at = modified_at
                
                newsDisplayViewController.title = dict["name"] as? String
                newsDisplayViewController.getNewsWithCategory(category: category_id, modified_at: modified_at)
            }
            let navCtrl:UINavigationController = UINavigationController(rootViewController: newsDisplayViewController)
            drawerCtrl.mainViewController = navCtrl;
            drawerCtrl.setDrawerState(.closed, animated: true)
            return
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0){
            return 160;
        }
        else{
            return 60;
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - CategoriesFromPlist
    func getFromPlist()->Array<Any> {
        let path = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]).appendingPathComponent("categories.plist").path
        let fileManager = FileManager.default
        if(fileManager.fileExists(atPath: path)){
            return NSArray(contentsOfFile: path) as! Array
        }
        else{
            return[]
        }
    }

}
