//
//  CommentViewController.swift
//  Ifuzone
//
//  Created by Manish on 17/11/16.
//  Copyright © 2016 Codeslay technologies. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import FloatRatingView
class CommentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FloatRatingViewDelegate, UITextViewDelegate {

    var commentsArray:Array<Any> = []
    var newsId:String = ""
    @IBOutlet var sendCommentView:UIView!
    @IBOutlet var floatRatingView: FloatRatingView!
    @IBOutlet var nameTF: UITextField!
    @IBOutlet var commentTV: UITextView!
    @IBOutlet var tableVeiw: UITableView!
    @IBOutlet var shadowImageView: UIImageView!
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableVeiw?.register(UINib(nibName: "CommentTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableVeiw.rowHeight = UITableViewAutomaticDimension
        tableVeiw.estimatedRowHeight = 50
        self.title = "Comments"
        // Do any additional setup after loading the view.
        self.getCommentsWithNews(news: newsId)
        self.setupRatingMethod()
        self.setupView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)

    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func setupRatingMethod(){
        self.floatRatingView.emptyImage = UIImage(named: "StarEmpty")
        self.floatRatingView.fullImage = UIImage(named: "StarFull")
        // Optional params
        self.floatRatingView.delegate = self
        self.floatRatingView.contentMode = UIViewContentMode.scaleAspectFit
        self.floatRatingView.maxRating = 5
        self.floatRatingView.minRating = 1
        self.floatRatingView.rating = 0
        self.floatRatingView.editable = true
        self.floatRatingView.halfRatings = true
        self.floatRatingView.floatRatings = false

    }
    
    func setupView(){
        self.commentTV.layer.cornerRadius = 5.0
        self.commentTV.layer.borderColor = UIColor.init(colorLiteralRed: 25.0/255, green: 83.0/255, blue: 104.0/255, alpha: 1).cgColor
        self.commentTV.layer.borderWidth = 1.0;
        self.commentTV.layer.masksToBounds = true
        
        self.nameTF.layer.cornerRadius = 5.0//25, 83,104
        self.nameTF.layer.borderColor = UIColor.init(colorLiteralRed: 25.0/255, green: 83.0/255, blue: 104.0/255, alpha: 1).cgColor
        self.nameTF.layer.borderWidth = 1.0;
        self.nameTF.layer.masksToBounds = true
        
        let innerShadowLayer:CALayer = CALayer.init()
        innerShadowLayer.contents = UIImage.init(named: "p2UQi")?.cgImage
        innerShadowLayer.contentsCenter = CGRect.init(x: 10.0/21.0, y: 10.0/21.0, width: 1.0/21.0, height: 1.0/21.0)
        self.sendCommentView.layer.insertSublayer(innerShadowLayer, at: 0)
        
        let shadowImage = UIImage(named: "p2UQi")
        let resizablebackImage = shadowImage?.resizableImage(withCapInsets: UIEdgeInsets(top:10,left:10,bottom:10,right:10))
        shadowImageView.image = resizablebackImage
        
        nameTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
        commentTV.text = "Your comment"
        commentTV.textColor = UIColor.lightGray
    }

    func getCommentsWithNews(news :String){
        MBProgressHUD.showAdded(to: self.view, animated: true)
        NetworkManager.getCommentsForNews(news: news){ (result:Array<Any>) in
            print(result)
            MBProgressHUD.hide(for: self.view, animated: true)
            if(result.count>0){
                self.commentsArray = result
                self.tableVeiw.reloadData()
                print(result)
            }
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - TableView Datasource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return (commentsArray.count);
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell:CommentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CommentTableViewCell
        let dict = commentsArray[indexPath.row] as! Dictionary <String,Any>
        cell.setupcell(commentDict: dict)
        return cell;
    }
    
    @IBAction func submitCommentButton(_ sender: UIButton){
//        if(!(nameTF.text?.isEmpty)!){
//            if((!commentTV.text.isEmpty) && (commentTV.text! != "Your comment")){
                if(floatRatingView.rating == 0){
                    let alert=UIAlertController(title: "Error", message: "Please rate the article", preferredStyle: UIAlertControllerStyle.alert);
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
                    {
                        action -> Void in
                    })
                    self.present(alert, animated: true, completion: nil)

                }
                else{
                    if((nameTF.text?.isEmpty)!){
                        nameTF.text = "Anonymous";
                    }
                    if((commentTV.text.isEmpty) || (commentTV.text! == "Your comment")){
                        commentTV.text = "";
                    }
                    
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                    commentTV.resignFirstResponder()
                    nameTF.resignFirstResponder()
                    NetworkManager.postCommentWithNewsId(newsId: self.newsId, name: self.nameTF.text!, comment: self.commentTV.text!, rating: self.floatRatingView.rating, completion: { (sucess:Bool) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        if(sucess){
                            let alert=UIAlertController(title: "", message: "Comment susessfully sent.", preferredStyle: UIAlertControllerStyle.alert);
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
                            {
                                action in self.getCommentsWithNews(news: self.newsId)
                                self.floatRatingView.rating = 0;
                                self.nameTF.text = ""
                                self.commentTV.text = "Your comment"
                                self.commentTV.textColor = UIColor.lightGray
                                
                            })
                            self.present(alert, animated: true, completion: nil)
                        }
                        else{
                            let alert=UIAlertController(title: "Error", message: "There is issue with the network. Please try again.", preferredStyle: UIAlertControllerStyle.alert);
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
                            {
                                action -> Void in
                            })
                            self.present(alert, animated: true, completion: nil)
                        }
                    })
                }
                
            }
//            else{
//                let alert=UIAlertController(title: "Error", message: "Please enter your comment", preferredStyle: UIAlertControllerStyle.alert);
//                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
//                {
//                    action -> Void in
//                })
//                self.present(alert, animated: true, completion: nil)
//
//            }
//        }
//        else{
//            let alert=UIAlertController(title: "Error", message: "Please enter your name", preferredStyle: UIAlertControllerStyle.alert);
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
//            {
//                action -> Void in
//            })
//            self.present(alert, animated: true, completion: nil)
//        }
//    }

    // MARK: - Float rating
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float){
        
    }
    
    // MARK: - TextView Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Your comment"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant = 0.0
            } else {
                self.keyboardHeightLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
