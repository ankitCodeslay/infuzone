//
//  CommentTableViewCell.swift
//  Ifuzone
//
//  Created by Manish on 17/11/16.
//  Copyright © 2016 Codeslay technologies. All rights reserved.
//

import UIKit
import FloatRatingView

class CommentTableViewCell: UITableViewCell {
    @IBOutlet var floatRatingView: FloatRatingView!
    @IBOutlet var name:UILabel!
    @IBOutlet var comment:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.floatRatingView.emptyImage = UIImage(named: "StarEmpty")
        self.floatRatingView.fullImage = UIImage(named: "StarFull")
        // Optional params
        self.floatRatingView.contentMode = UIViewContentMode.scaleAspectFit
        self.floatRatingView.maxRating = 5
        self.floatRatingView.minRating = 1
        self.floatRatingView.editable = false
        self.floatRatingView.halfRatings = false
        self.floatRatingView.floatRatings = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupcell(commentDict:Dictionary<String, Any>){
        self.floatRatingView.rating = commentDict["rating"] as! Float
        self.comment.text = commentDict["comment"] as? String
        if((commentDict["name"] as? String)?.characters.count==0){
            self.name.text = "."
        }
        else{
            self.name.text = commentDict["name"] as? String
        }

    }
}
