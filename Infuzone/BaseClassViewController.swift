//
//  BaseClassViewController.swift
//  Ifuzone
//
//  Created by Manish on 10/11/16.
//  Copyright © 2016 Codeslay technologies. All rights reserved.
//

import UIKit
import KYDrawerController

class BaseClassViewController: UIViewController {

    weak var drawerCtrl:KYDrawerController?
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        drawerCtrl = appDelegate.window!.rootViewController as? KYDrawerController
        // Do any additional setup after loading the view.
    }
    
    func setupMenuIcon(){
        let menuBtn = UIBarButtonItem.init(image: UIImage.init(named: "category_icon"), style: UIBarButtonItemStyle.done, target: self, action: #selector(toggleNavigation))
        let leftText = UIBarButtonItem.init(title: "Category", style: UIBarButtonItemStyle.done, target: nil, action: nil)
        leftText.setTitleTextAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 24)], for: UIControlState.normal)
        self.navigationItem.leftBarButtonItems = [menuBtn]
    }
    
//    func setuptextItems

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func toggleNavigation(){
        if (drawerCtrl?.drawerState == .opened) {
            drawerCtrl?.setDrawerState(.closed, animated: true)
        }
        else if (drawerCtrl?.drawerState == .closed){
            drawerCtrl?.setDrawerState(.opened, animated: true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
