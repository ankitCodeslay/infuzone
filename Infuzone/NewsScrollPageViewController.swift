//
//  NewsScrollPageViewController.swift
//  Ifuzone
//
//  Created by Manish on 16/11/16.
//  Copyright © 2016 Codeslay technologies. All rights reserved.
//

import UIKit
import KYDrawerController
import MBProgressHUD

class NewsScrollPageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    weak var drawerCtrl:KYDrawerController?
    var category_id:String?
    var modified_at:String?
    var newsArray:Array<Any>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        drawerCtrl = appDelegate.window!.rootViewController as? KYDrawerController
        self.setupMenuIcon()
        // Do any additional setup after loading the view.
        self.edgesForExtendedLayout = []
//        self.getNewsWithCategory(category: category_id!)
    }
    
    func getNewsWithCategory(category :String, modified_at:String){
        newsArray = []
        MBProgressHUD.showAdded(to: self.view, animated: true)
        NetworkManager.getNewsForCategory(category: category, modified_at: modified_at){ (result:Array<Any>) in
            print(result)
            MBProgressHUD.hide(for: self.view, animated: true)
            if(result.count>0){
                self.newsArray = result
                let newsDisplayViewController:NewsDisplayViewController = NewsDisplayViewController(nibName: "NewsDisplayViewController", bundle: nil)
                
                var newsObject:Dictionary<String,Any> = self.newsArray?[0] as! Dictionary<String, Any>
                newsDisplayViewController.rating = newsObject["rating"] as? Float
                newsDisplayViewController.imageUrl = newsObject["image"] as? String
                newsDisplayViewController.newsTitleText = newsObject["title"] as? String
                newsDisplayViewController.newsBodyText = newsObject["body"] as? String
                newsDisplayViewController.newsSourceText = newsObject["source_url"] as? String
                newsDisplayViewController.index = 0
                newsDisplayViewController.commentCountNumber = newsObject["comment_count"] as? Int
                newsDisplayViewController.newsId = String(newsObject["id"] as! Int)
                newsDisplayViewController.navigationControllerToLink = self.navigationController
                let viewControllers = [newsDisplayViewController]
                self.setViewControllers(viewControllers, direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
                self.delegate = self;
                self.dataSource = self;
            }
        }
    }
    
    // MARK: - MenuMethods Methods
    
    func setupMenuIcon(){
        let menuBtn = UIBarButtonItem.init(image: UIImage.init(named: "category_icon"), style: UIBarButtonItemStyle.done, target: self, action: #selector(toggleNavigation))
        let leftText = UIBarButtonItem.init(title: "Category", style: UIBarButtonItemStyle.done, target: nil, action: nil)
        leftText.setTitleTextAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 24)], for: UIControlState.normal)
        self.navigationItem.leftBarButtonItems = [menuBtn]
    }
    
    func toggleNavigation(){
        if (drawerCtrl?.drawerState == .opened) {
            drawerCtrl?.setDrawerState(.closed, animated: true)
        }
        else if (drawerCtrl?.drawerState == .closed){
            drawerCtrl?.setDrawerState(.opened, animated: true)
        }
    }
    
    // MARK: - PageViewController Methods
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?{
        let index = (viewController as! NewsDisplayViewController).index! - 1
        if(index>0){
            let newsDisplayViewController:NewsDisplayViewController = NewsDisplayViewController(nibName: "NewsDisplayViewController", bundle: nil)
            
            var newsObject:Dictionary<String,Any> = self.newsArray?[index] as! Dictionary<String, Any>
            newsDisplayViewController.rating = newsObject["rating"] as? Float
            newsDisplayViewController.imageUrl = newsObject["image"] as? String
            newsDisplayViewController.newsTitleText = newsObject["title"] as? String
            newsDisplayViewController.newsBodyText = newsObject["body"] as? String
            newsDisplayViewController.newsSourceText = newsObject["source_url"] as? String
            newsDisplayViewController.index = index
            newsDisplayViewController.commentCountNumber = newsObject["comment_count"] as? Int
            newsDisplayViewController.newsId = String(newsObject["id"] as! Int)
            newsDisplayViewController.navigationControllerToLink = self.navigationController
            return newsDisplayViewController
        }
        return nil
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?{
        let index = (viewController as! NewsDisplayViewController).index! + 1
        if(index < (newsArray?.count)!){
            let newsDisplayViewController:NewsDisplayViewController = NewsDisplayViewController(nibName: "NewsDisplayViewController", bundle: nil)
            
            var newsObject:Dictionary<String,Any> = self.newsArray?[index] as! Dictionary<String, Any>
            newsDisplayViewController.rating = newsObject["rating"] as? Float
            newsDisplayViewController.imageUrl = newsObject["image"] as? String
            newsDisplayViewController.newsTitleText = newsObject["title"] as? String
            newsDisplayViewController.newsBodyText = newsObject["body"] as? String
            newsDisplayViewController.newsSourceText = newsObject["source_url"] as? String
            newsDisplayViewController.index = index
            newsDisplayViewController.commentCountNumber = newsObject["comment_count"] as? Int
            newsDisplayViewController.newsId = String(newsObject["id"] as! Int)
            
            newsDisplayViewController.navigationControllerToLink = self.navigationController
            return newsDisplayViewController
        }
        return nil
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
