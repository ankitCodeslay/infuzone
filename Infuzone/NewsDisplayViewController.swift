//
//  NewsDisplayViewController.swift
//  Ifuzone
//
//  Created by Manish on 10/11/16.
//  Copyright © 2016 Codeslay technologies. All rights reserved.
//

import UIKit
import FloatRatingView
import AlamofireImage

class NewsDisplayViewController: BaseClassViewController, FloatRatingViewDelegate {
    
    @IBOutlet var floatRatingView: FloatRatingView!
    @IBOutlet var newsImage: UIImageView!
    @IBOutlet var newsTitle: UILabel!
    @IBOutlet var newsBody: UILabel!
    @IBOutlet var newsSource: UIButton!
    @IBOutlet var commentCountBtn: UIButton!
    var newsId:String = ""
    weak var navigationControllerToLink: UINavigationController?
    
    var rating:Float?
    var imageUrl:String?
    var newsTitleText:String?
    var newsBodyText:String?
    var newsSourceText:String?
    var commentCountNumber:Int?
    var index:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupRatingMethod()
        self.setupViewData()
    }

    func setupRatingMethod(){
        self.floatRatingView.emptyImage = UIImage(named: "StarEmpty")
        self.floatRatingView.fullImage = UIImage(named: "StarFull")
        // Optional params
        self.floatRatingView.delegate = self
        self.floatRatingView.contentMode = UIViewContentMode.scaleAspectFit
        self.floatRatingView.maxRating = 5
        self.floatRatingView.minRating = 1
        self.floatRatingView.editable = false
        self.floatRatingView.halfRatings = true
        self.floatRatingView.floatRatings = false
    }
    
    func setupViewData(){
        newsBody.text = self.newsBodyText
        newsTitle.text = self.newsTitleText
        newsSource.setTitle(self.newsSourceText, for: UIControlState.normal)
        self.floatRatingView.rating = self.rating!
        let url = URL(string: kImageBaseURL+imageUrl!)!
        let placeholderImage = UIImage(named: "logo")!
        newsImage?.af_setImage(withURL: url, placeholderImage: placeholderImage)
        commentCountBtn.setTitle((" " + "\(commentCountNumber!)" + " Comments"), for: UIControlState.normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Float rating
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float){
        
    }

    // MARK: - Share Button
    
    @IBAction func shareTextButton(_ sender: UIButton) {
        
        // text to share
        let text = "This is some text that I want to share."
        
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    // MARK: - Comment Button
    @IBAction func commentButton(_ sender: UIButton){
        let commentViewController:CommentViewController = CommentViewController(nibName: "CommentViewController", bundle: nil)
        commentViewController.newsId = newsId
        navigationControllerToLink?.pushViewController(commentViewController, animated: true)
    }
    
    @IBAction func sourceTextButton(_ sender: UIButton){
        UIApplication.shared.openURL(URL(string: newsSourceText!)!)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
