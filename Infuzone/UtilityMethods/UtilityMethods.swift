//
//  UtilityMethods.swift
//  Ifuzone
//
//  Created by Manish on 10/11/16.
//  Copyright © 2016 Codeslay technologies. All rights reserved.
//

import UIKit

class UtilityMethods: NSObject {
    class func primaryColor()->UIColor{
        return UIColor(colorLiteralRed: 13.0/255, green: 99.0/255, blue: 128.0/255, alpha: 1.0)
    }
}
